#!/usr/bin/env bash

SITES="http://someonewhocares.org/hosts/hosts
http://www.malwaredomainlist.com/hostslist/hosts.txt
https://adaway.org/hosts.txt
http://hosts-file.net/ad_servers.txt
http://winhelp2002.mvps.org/hosts.txt
http://pgl.yoyo.org/adservers/serverlist.php -d hostformat=hosts -d showintro=0 -d mimetype=plaintext"

rm -f ./hosts.tmp
echo "$SITES" | while read line; do
	curl -s $line | grep -E '^127.0.0.1|^0.0.0.0' | grep -v 'localhost' | awk '{print $2}' >> ./hosts.tmp
done

cat /etc/hosts > ./hosts.backup
sort -u ./hosts.tmp | sed "s/^/127.0.0.1\t/" >> ./hosts.tmp.uniq
cat ./hosts.backup ./hosts.tmp.uniq | sort -u > /etc/hosts

	