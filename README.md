# README #

### What is this script for? ###

* To block annoying advertisement on sites
* The script makes a new /etc/hosts file with more popular domains from advertisement affiliate programs to ensure, that all requests from your PC to a site with any advertisement blocks will be forward you to the loopback address (127.0.0.1) and by this - blocks these domains.

### How to use on Linux? ###

Clone the repo:
```shell
$ sudo git clone https://bitbucket.org/gunb1rd/ad-free /opt/ad-free
```
Backup your old hosts file:
```shell
$ sudo cp /etc/hosts /etc/hosts.orig
```
Run the script:
```shell
$ sudo /opt/ad-free/hostsgen.sh
```
Ensure, that the script works correctly. Read some lines from the hosts file:
```shell
$ head /etc/hosts
127.0.0.1	005.free-counter.co.uk
127.0.0.1	006.free-adult-counters.x-xtra.com
127.0.0.1	006.free-counter.co.uk
127.0.0.1	006.free-counter.co.uk
127.0.0.1	006.freecounters.co.uk
127.0.0.1	0075-7112-e7eb-f9b9.reporo.net
127.0.0.1	007.free-counter.co.uk
127.0.0.1	007.go2cloud.org
127.0.0.1	008.free-counter.co.uk
127.0.0.1	008.free-counters.co.uk
```
Make a cronjob:
```shell
$ sudo crontab -e
```
and add this line: (update the hosts file each hour)
```
0	*/1	*	*	*	/opt/ad-free/hostsgen.sh
```

Enjoy!